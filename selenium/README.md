# TP Automatisation de tests fonctionnels avec Selenium IDE 

Sur un projet informatique, il est courant qu’une application nécessite la livraison de plusieurs versions de développement. 
Les méthodes agiles accentuent d’ailleurs cette tendance. A chaque livraison de l’application, il est nécessaire d’effectuer des tests de non régressions. 

Il est alors nécessaire d’outiller le test pour rejouer plusieurs fois les scénarii définis à des instants différents et assurer ainsi la non régression de l’application mise sous test. 

L’automatisation nous permet alors de : 
 - Gagner en productivité en réduisant les temps d’exécution de tests
 - Rationnaliser les tests
 - Accroître la qualité en testant plus
 - Etre réactif: les tests peuvent être effectués à chaque livraison/paramétrage
 - Systématiser le test de non régression
 - Multiplier les opportunités d’amélioration de la qualité de service 

Ces objectifs sont autant de bonnes raisons pour ne pas effectuer manuellement les tests et laisser un robot exécuter les tâches fastidieuses et répétitives.

L’objectif du TP est comprendre comment outiller un test fonctionnel afin de tester l’application et surtout permettre de rejouer la séquence de test afin d’assurer les tests de non régression de notre application. Pour cela, nous allons utiliser un plugin Firefox destiné aux tests : Selenium IDE.

## Selenium IDE

> http://www.seleniumhq.org/projects/ide/


 ![Selenium IDE](./image1.jpg)

Selenium IDE est un outil simple et facile d’utilisation qui permet de répondre aux problématiques de capture de scénario puis de reproduction (rejeu) de ce scénario. Il est principalement utilisé pour effectuer des tests de non régression fonctionnels.

Il s’interface également avec les 2 autres projets Selenium WebDriver et Selenium RC que nous verrons à la fin du TP (pour les plus rapides).

Selenium IDE est disponible sous la forme d’un plugin Firefox ou Chrome et reste le moyen le plus efficace pour développer des scénarios de test rapidement. Les scénarios sont rédigés en HTML, mais il est possible de développer les scripts sous la forme de programmes qui pilotent le navigateur (Ruby, Python, Java, PHP, Javascript, etc). Ce TP ne présentera que la forme HTML.

Il permet de : 
 - Enregistrer automatiquement les actions effectuées par l’utilisateur
 - Reconnaitre et localiser des éléments dans la page
 - Rejouer le test
 - Disposer d’un résultat de test
 - Sauvegarder des cas de test et suite de tests

## Installer Selenium IDE

 - Pour Firefox : https://addons.mozilla.org/fr/firefox/addon/selenium-ide/
 - Pour Chrome : https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd

## Interface Graphique de Selenium IDE

 ![Selenium IDE](./image3.png)

 1. Enregistre les actions navigateur de l'utilisateur,
 2. Exécute la suite de tests ou le test sélectionné, 
 3. Volet des opérations enregitrées
 4. Volet de création de ou de modification d'opérations 

## A faire

Créer les suites de tests et les cas de tests référencés dans le référentiel d'exigences

Enregistrer et livrer le projet