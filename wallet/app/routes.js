var fs = require("fs");
var _ = require('lodash');

var message = '';
var chaine = '';

var regexPwd = '^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{6,}$';
var regexUrl = '^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$';

var confToken = "Admin12345";

module.exports = function (app) {
 
  //-------------fonction--------------------
  //return an array of objects according to key, value, or key and value matching
  function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;
      if (typeof obj[i] == 'object') {
        objects = objects.concat(getObjects(obj[i], key, val));
      } else
      //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
      if (i == key && obj[i].toLowerCase().includes(val.toLowerCase()) || i == key && val == '') { 
        objects.push(obj);
      } else if (obj[i] == val && key == '') {
        //only add if the object is not already in the array
        if (objects.lastIndexOf(obj) == -1) {
          objects.push(obj);
        }
      }
    }
    return objects;
  }

  //return an array of values that match on a certain key
  function getValues(obj, key) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;
      if (typeof obj[i] == 'object') {
        objects = objects.concat(getValues(obj[i], key));
      } else if (i == key) {
        objects.push(obj[i]);
      }
    }
    return objects;
  }

  //return an array of keys that match on a certain value
  function getKeys(obj, val) {
    var objects = [];
    for (var i in obj) {
      if (!obj.hasOwnProperty(i)) continue;
      if (typeof obj[i] == 'object') {
        objects = objects.concat(getKeys(obj[i], val));
      } else if (obj[i] == val) {
        objects.push(i);
      }
    }
    return objects;
  }

  //fonction de gestion CORS
  function accesscontrol(res){
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With , content-type, origin');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
  }

  //fonction de vérification si l'utilisateur est autorisé à faire des modifications 
  function verifAuth(auth) {
    if ((auth == confToken)) {
      //on retourne vrai s'il est autorisé
      return true;
    }
    else {
      //on retourne faux s'il n'est pas autorisé
      return false;
    }
  }

  // function checkRegEx(), pour controler la validité d'un champ
  function checkRegEx(strReq, type, nberr) {
    if(type == 'url'){
      if ((strReq && !strReq.trim().match(regexUrl))) {
        nberr++;
        message = 'L\'URL n\'est pas conforme';
      }
    }else if(type == 'pwd'){
      if ((strReq && !strReq.trim().match(regexPwd))) {
        nberr++;
        message = 'Le mot de passe doit avoir: une majuscule, une minuscule et un chiffre, avec une longueur minimum de 6 caractères';
      }
    }
    return nberr;
  }
  
  //-------------API--------------------
  /**
  * Delete all wallets
  *
  * @section admin wallets
  * @type delete
  * @url /api/deleteall
  * 
  * @param {header} Authorization 
  * 
  */
  app.delete('/api/deleteall', function (req, res) {
    var auth = req.headers.authorization;   
    if (!auth) {     // Si aucun header n'est passé, on cherche à servir une 401, on ne passe pas en retour le header WWW-Authenticate pour ne pas avoir la popup d'authentification du navigateur
      message = "Il faut être authentifié";
      res.status(401).send(message);
    } else if (auth) {    //si le header authorization est présent on cherche à le valkeyer
      var isAuthorized = verifAuth(auth);
      if (isAuthorized) {
        //si l'authentification est correcte
        chaine = '{"data":[]}';
        fs.writeFileSync("data/wallet.json", chaine, "UTF-8");
        message = '';
        res.sendStatus(200);
      }
      // si l'itulisateur n'est pas autorisé
    } else {
      message = "Le token est incorrect";
      res.status(401).send(message);
    }
  });

  /**
  * Initialize wallets
  *
  * @section admin wallets
  * @type delete
  * @url /api/datatest.
  * 
  * @param {header} Authorization 
  * 
  */
  app.delete('/api/datatest', function (req, res) {
    var auth = req.headers.authorization;   
    if (!auth) {     // Si aucun header n'est passé, on cherche à servir une 401, on ne passe pas en retour le header WWW-Authenticate pour ne pas avoir la popup d'authentification du navigateur
      message = "Il faut être authentifié";
      res.status(401).send(message);
    } else if (auth) {    //si le header authorization est présent on cherche à le valkeyer
      var isAuthorized = verifAuth(auth);
      if (isAuthorized) {
        //si l'authentification est correcte
        chaine = '{"data":[{"key":"URL1","url":"http://www.google.fr","login":"Pierre","pwd":"Abcde1","comment":"compte perso","time":1482963573074},{"key":"URL2","url":"http://www.laposte.net","login":"Rene","pwd":"1234Ab","comment":"compte pro","time":1482963592496}]}';
        fs.writeFileSync("data/wallet.json", chaine, "UTF-8");    
        message = "Le fichier de wallet a été reinitialisé";
        res.status(200).send(message);
      }
      // si l'itulisateur n'est pas autorisé
    } else {
      message = "Le token est incorrect";
      res.status(401).send(message);
    }
  });

  /**
  * Search wallet
  *
  * @section wallets
  * @type get
  * @url /api/rechercher
  * 
  * @param {query =} mode 
  * @param {query =} login 
  * @param {query =} url 
  * @param {query =} key 
  * 
  */
  app.get('/api/rechercher', function (req, res) {
    //ouverture de la liste complete de wallet   
    var listeWallet = JSON.parse(fs.readFileSync("data/wallet.json", "UTF-8"));
    accesscontrol(res);
    if (req.query.mode == 'all') {
      res.json(listeWallet.data.sort(function(b, a) {
        return parseFloat(a.time) - parseFloat(b.time);
      })
      );
    }
    else if (req.query.login) {
      var filtrelisteWallet = getObjects(listeWallet, 'login', req.query.login);
      //example of grabbing objects that match some key and value in JSON
      console.log(getObjects(listeWallet, 'login', req.query.login));
      res.json(filtrelisteWallet.sort(function(b, a) {
        return parseFloat(a.time) - parseFloat(b.time);
      })
      );
    }
    else if (req.query.url) {
      var filtrelisteWallet = getObjects(listeWallet, 'url', req.query.url);
      //example of grabbing objects that match some key and value in JSON
      console.log(getObjects(listeWallet, 'url', req.query.url));
      res.json(filtrelisteWallet.sort(function(b, a) {
        return parseFloat(a.time) - parseFloat(b.time);
      })
      );
    }
    else if (req.query.key) {
      var filtrelisteWallet = getObjects(listeWallet, 'key', req.query.key);
      //example of grabbing objects that match some key and value in JSON
      console.log(getObjects(listeWallet, 'key', req.query.key));
      res.json(filtrelisteWallet.sort(function(b, a) {
        return parseFloat(a.time) - parseFloat(b.time);
      })
      );
    }
    else {
      console.log('La requete est incorrecte');
      res.sendStatus(400)
    }
  });

  /**
  * Add wallet
  *
  * @section wallets
  * @type post
  * @url /api/ajouter
  * 
  * @param {query} key 
  * @param {query} url 
  * @param {query =} login 
  * @param {query =} pwd 
  * @param {query =} comment 
  *
  */
  app.post('/api/ajouter', function (req, res) {
    var listeWallet = JSON.parse(fs.readFileSync("data/wallet.json", "UTF-8"));
    var nberr = 0
    var status;

    // Traitement des cas d'erreur
    // check regex url
    nberr = checkRegEx(req.query.url,'url', nberr);
    // check regex pwd
    nberr = checkRegEx(req.query.pwd,'pwd', nberr);
    
    if(nberr != 0){
      status = 406;
    }

    if (_.isEmpty(req.query.key)) {
      nberr++;
      message = 'La clef est obligatoire';
      status = 400;
    }
    else {
      //Recherche de l'index 
      var index = -1;
      var filteredObj = listeWallet.data.find(function (item, i) {
        if (item.key === req.query.key) {
          index = i;
          return i;
        }
      });
      if (index != -1) {
        nberr++;
        message = 'La clef existe déjà';
        status = 409;
      }
    }

    if (nberr == 0) {
      //Ajout du wallet dans la liste
      var horodate = new Date().getTime();
      listeWallet.data.push({ key: req.query.key, url: req.query.url, login: req.query.login, pwd: req.query.pwd, comment: req.query.comment, time: horodate });
      // Enregistrement dans la persistance
      chaine = JSON.stringify(listeWallet);
      fs.writeFileSync("data/wallet.json", chaine, "UTF-8");
      message = "Le wallet -"+ req.query.key +"- a bien été ajouté." ;
      res.status(201).send(message);
    }
    else {
      console.log(message);
      res.status(status).send(message);
    }
  }
  );

  /**
  * Update wallet
  *
  * @section wallets
  * @type put
  * @url /api/modifier
  * 
  * @param {query} key 
  * @param {query} url 
  * @param {query =} login 
  * @param {query =} pwd 
  * @param {query =} comment 
  *
  */
  app.put('/api/modifier', function (req, res) {
    var listeWallet = JSON.parse(fs.readFileSync("data/wallet.json", "UTF-8"));
    var nberr = 0
    var status;

    // Traitement des cas d'erreur
    // check regex url
    nberr = checkRegEx(req.query.url,'url', nberr);
    // check regex pwd
    nberr = checkRegEx(req.query.pwd,'pwd', nberr);

    if(nberr != 0){
      status = 406;
    }

    if (_.isEmpty(req.query.key)) {
      nberr++;
      message = 'La clef est obligatoire';
      status = 400;
    }
    else {
      //Recherche de l'index 
      var index = -1;
      var filteredObj = listeWallet.data.find(function (item, i) {
        if (item.key === req.query.key) {
          index = i;
          return i;
        }
      });
      if (index == -1) {
        nberr++;
        message = 'La clef n\' a pas été trouvée';
        status = 404;
      }
    }

    if (nberr == 0) {
      //Suppression du wallet dans la liste
      listeWallet.data.splice(index, 1);
      //Ajout du wallet dans la liste
      var horodate= new Date().getTime();
      listeWallet.data.push({ key: req.query.key, url: req.query.url, login: req.query.login, pwd: req.query.pwd, comment: req.query.comment, time: horodate  });
      // Enregistrement dans la persistance
      chaine = JSON.stringify(listeWallet);
      fs.writeFileSync("data/wallet.json", chaine, "UTF-8");
      message = "Le wallet a bien été modifié";
      res.status(200).send(message);
    }
    else {
      console.log(message);
      res.status(status).send(message);
    }
  });

  /**
  * Delete one wallet
  *
  * @section wallets
  * @type delete
  * @url /api/supprimer
  * 
  * @param {header} Authorization 
  * @param {query} key 
  *
  */
  app.delete('/api/supprimer', function (req, res) {
    // On récupère le header authorization basic en base64
    var auth = req.headers.authorization;   
    if (!auth) {     // Si aucun header n'est passé, on cherche à servir une 401, on ne passe pas en retour le header WWW-Authenticate pour ne pas avoir la popup d'authentification du navigateur
      message = "Il faut être authentifié";
      res.status(401).send(message);
    } else if (auth) {    //si le header authorization est présent on cherche à le valkeyer
      var isAuthorized = verifAuth(auth);
      if (isAuthorized) {
        //si l'authentification est correcte
        if (!_.isEmpty(req.query.key)) {
          console.log('Suppression du clef '+req.query.key+' en cours');
          var listeWallet = JSON.parse(fs.readFileSync("data/wallet.json", "UTF-8"));
          //Recherche de l'index correspondant au wallet à supprimer.
          var index = -1;
          var filteredObj = listeWallet.data.find(function (item, i) {
            if (item.key === req.query.key) {
              index = i;
              return i;
            }
          });

          if (index == -1) {
            message = 'le wallet n\'a pas été trouvé';
            res.status(400).send(message);
            console.log('pas de clef trouvée')
          }
          else {
          //Suppression du wallet dans la liste si on troyve bien un key
            listeWallet.data.splice(index, 1);
            // Enregistrement dans la persistance
            chaine = JSON.stringify(listeWallet);
            fs.writeFileSync("data/wallet.json", chaine, "UTF-8");
            message = 'Le wallet a bien été supprimé';
            res.status(200).send(message);             
          }
        }
        else {
          message = 'le wallet n\'a pas été trouvé';
          res.status(400).send(message);
          console.log('pas de clef trouvée');
        }
        // si l'itulisateur n'est pas autorisé
      } else {
        message = "Le token est incorrect";
        res.status(401).send(message);
      }
    }

  });

  // application -------------------------------------------------------------
  app.get('/', function (res) {
    res.sendFile(__dirurl + '/public/index.html'); 
  });

};
