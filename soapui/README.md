# TP Automatisation de tests API avec SoapUI

## SoapUI

> https://www.soapui.org/


SoapUI est une application open source permettant le test de web service dans une architecture orientée services (SOA). Ses fonctionnalités incluent l'inspection des web service, l'invocation, le développement, la simulation, le mocking, les tests fonctionnels, les tests de charge et de conformité.



## Interface Graphique de Selenium IDE

 ![soapUI](./soapui.png)


## A faire

Créer les suites de tests et les cas de tests référencés dans le référentiel d'exigences

Enregistrer et livrer le projet